package Lesson5;

import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 14.12.15.
 */
public class ue5_1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int n = input.nextInt();
            System.out.println("RETURN: "+getFactorial(n));
        }catch (InputMismatchException e){
            System.out.println("Exeption "+e);
        }

    }
    public static int getFactorial(int n){
        int x = 0;
        if(n > 0){
            x = n*getFactorial(n-1);
        }else {
            return 1;
        }
        return x;
    }

    public static int itFactorial(int n){
        int x = 1;
        if(n > 0){
            for (int i =n; i > 0; i--){
                x = x+i;
            }
        }else {
            return 1;
        }
        return x;
    }
}
