package Lesson6.Classes;

import java.io.*;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 25.01.16.
 */
public class ue6_3 {
    public static void main(String[] args) throws InputMismatchException {
        System.out.println("Java create new File");
        System.out.println("Enter File Name");

        Scanner input = new Scanner(System.in);
        String fileName = input.next();
        //Get current Path
        String current = "/Users/ric/Desktop/";
        System.out.println("Dir to write File: "+current);
        //create file with current path
        File myfile = new java.io.File(current+fileName+".txt");
        //use Buffered Writer to write to File
        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter(myfile));
            System.out.println("Enter content to be written in file");
            String content = input.next();
            writer.write (content);
            writer.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
