package Lesson6.Classes;

import java.util.InputMismatchException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 25.01.16.
 */
public class ue6_6 {
    public static void main(String[] args) throws InputMismatchException {
        Scanner input = new Scanner(System.in);
        int n = 0;
        int max;
        System.out.println("Enter Maximum values");
        max = input.nextInt();
        LinkedList<Integer> meineListe = new LinkedList<Integer>();
        System.out.println("Filling List with raising Values");

        do {
            meineListe.add(n++);
        }while (n < max );

        System.out.println( "printed list: "+printList(meineListe));

    }
    public static String printList(LinkedList list){
        String out = "";
        int i;
        int max = list.size();

        for (i = 0; i <max; i++){
            out = out+ list.get(i)+ "\n";
        }
        return out;


    }

}
