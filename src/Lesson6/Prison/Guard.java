package Lesson6.Prison;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 18.01.16.
 */
public class Guard {

    private int age;
    private boolean isSick;

    public Guard(int age){
        //define the age of the guard
        setAge(age);

    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void nextDay(){


    }
}
