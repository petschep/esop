package Lesson4;

import java.util.Arrays;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 07.12.15.
 */
public class ue4_2{
    public static void main(String[] args) {

        int [][] myArrayINT = new int [][]{{0,1,2},{0,1,2},{0,1,2}};
        boolean [][] myArrayBOOL = new boolean[][]{{true,false},{true,false},{true,false},{false,true},{false,true},{true,false},{false,true}};
        char [][] myArrayCHAR = new char[][]{{'\uDC00','\uDC00'},{'\uDC00'},{'\uDC00'}};
        int [][] myArrayINT2 = new int [][]{{0,1,2,3},{0,1,2},{0,1},{0}};

        print2DArrayToScreen(myArrayINT);
    }
    static void print2DArrayToScreen(int[][] var){
        System.out.println("Array is: "+ Arrays.deepToString(var));

    }
}
