package Lesson4;

import java.util.InputMismatchException;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 29.12.15.
 */
public class ue4_4 {
    public static void main(String[] args) {
        int c;
        int r;
        Scanner input = new Scanner(System.in);
        try{
            System.out.println("Define Columns:");
            c = input.nextInt();
            System.out.println("Define Rows");
            r = input.nextInt();
            System.out.println("Array is: "+ Arrays.deepToString(create2DArray(r, c)));

        }catch (InputMismatchException e){
            System.out.println("Exeption detected: ");
            throw e;
        }
    }

    /**
     * Returns 2D Array
     * @param r
     * @param c
     * @return
     */
    public static String[][] create2DArray(int r, int c){
        //define temporary Array
        String[][] tmpArray = new String[r][c];
        for (int i = 0; i < r; i++){
            for (int e = 0; e < c; e++){
                tmpArray[i][e] = randomString();
            }
        }
        return tmpArray;

    }

    /**
     * Returns Random String from 0 to Z
     * @return
     */
    public static String randomString(){
        char x = 0;
        final String alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final int l = alphabet.length();

        Random r = new Random();

        for (int i = 0; i < 50; i++) {
            x = alphabet.charAt(r.nextInt(l));
        }
        String z = Character.toString(x);
        return z;
    }
}
