package Lesson4;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 29.12.15.
 */
public class ue4_3 extends ue4_2{
    public static void main(String[] args) {
        int[][] intArray = new int[][]{{1,2,3},{4,5,6},{7,8,9}};
        int[][] matrix = transposeMatrix(intArray);
        print2DArrayToScreen(matrix);

    }

    /**
     * Transforms Matrix
     * @param matrix
     */
    public static int[][] transposeMatrix(int[][] matrix){
        int m = matrix.length;
        int n = matrix[0].length;

        int[][] trasposedMatrix = new int[n][m];

        for(int x = 0; x < n; x++)
        {
            for(int y = 0; y < m; y++)
            {
                trasposedMatrix[x][y] = matrix[y][x];
            }
        }

        return trasposedMatrix;
    }
}
