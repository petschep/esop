package Lesson4;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 07.12.15.
 */
public class ue4_1 {
    public static void main(String[] args) {
        int [][] myArrayINT = new int [][]{{0,1,2},{0,1,2},{0,1,2}};
        boolean [][] myArrayBOOL = new boolean[][]{{true,false},{true,false},{true,false},{false,true},{false,true},{true,false},{false,true}};
        char [][] myArrayCHAR = new char[][]{{'\uDC00','\uDC00'},{'\uDC00'},{'\uDC00'}};
        int [][] myArrayINT2 = new int [][]{{0,1,2,3},{0,1,2},{0,1},{0}};
    }
}
