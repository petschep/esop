package Lesson4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 30.12.15.
 */
public class ue4_5{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int d;
        String [] board;
        try{
            System.out.println("Enter a dimension for a chessboard (maximum is 26)");
            do {
              d = input.nextInt();
            }while (d > 26 || d < 0);

            board = chessBoard(d);
            System.out.println(Arrays.deepToString(board));

        }catch (InputMismatchException e){
            System.out.println("Exeption thrown:");
            throw e;
        }
    }

    /**
     *
     * @param dimension
     * @return
     */
    public static String[] chessBoard(int dimension){
        String [] tmp = new String[dimension];
        int h, v;
        char [] abc = alphabeth();
        String out = "";
        //Loop through and return Line by line
        for(v = 0; v< dimension;){
            //out = out+out;
            for (h=0; h<dimension && v<dimension; h++){
                tmp[v] = tmp[v]+" "+abc[v]+h;
            }
            v++;
        }
        return tmp;

    }
    /**
     * Method that returns the Alphaeth
     * @return
     */
    public static char[] alphabeth(){
        char[] alpha = new char[26];
        int k = 0;
        for(int i = 0; i < 26; i++){
            alpha[i] = (char)(97 + (k++));
        }
        return alpha;
    }

}
