package Lesson2;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author: Patric Petscher <bitbucket.org/petschep>
 * created on 23.10.15.
 */
public class ue2_7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try{
            int i;
            int n, x,z;
            System.out.println("Enter any Integer Value");
            x = input.nextInt();
            System.out.println("Enter any Integer Value of how many rounds should be calculated");
            n = input.nextInt();

            z = 1;
            System.out.println("Entered Int " + x);
            for (i=1; i<=n; i++){
                z = z*i;
            }
            System.out.println("Calculated Number from "+x+" for "+n+" additions is " + z);
        }catch (InputMismatchException e){
            System.out.println("Error entered wrong type, restart Application!");
        }
    }
}
