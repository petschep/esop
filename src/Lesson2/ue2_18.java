package Lesson2;

import java.util.Arrays;
/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 10.11.15.
 */
public class ue2_18 {
    public static void main(String[] args) {
        int i,c,tmp;
        int [] myArray = new int[]{2,6,9};
        i = 0;
        c = myArray.length - 1;
        for (i = 0; i < myArray.length / 2; i++, c--) {
            tmp = myArray[i];
            myArray[i] = myArray[c];
            myArray[c] = tmp;
        }
        System.out.println("Array {2,6,9} reverted to "+ Arrays.toString(myArray));
    }
}
