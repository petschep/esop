package Lesson2;

import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * @author: Patric Petscher <bitbucket.org/petschep>
 * created on 23.10.15.
 */
public class ue2_4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter any Integer Value");
        try{
            int x,y,z,sum;
            z=0;
            x = input.nextInt();
            System.out.println("Entered Int " + x);
            for (y=1; y<=x; y++){
                z = z+y;
            }
            sum = z/x;
            System.out.println("arithmethic center = " +sum );
        }catch (InputMismatchException e){
            System.out.println("Error entered wrong type, restart Application!");
        }
    }
}
