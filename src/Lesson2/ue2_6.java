package Lesson2;

import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * @author: Patric Petscher <bitbucket.org/petschep>
 * created on 23.10.15.
 */
public class ue2_6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter any Integer Value");
        try{
            int i;
            long x,z;
            x = input.nextInt();
            z = 1;
            System.out.println("Entered Int " + x);
            for (i=1; i<=x; i++){
                z = z*i;
            }
            System.out.println("Calculated Number = " + z);
        }catch (InputMismatchException e){
            System.out.println("Error entered wrong type, restart Application!");
        }
    }
}
