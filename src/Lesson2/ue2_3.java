package Lesson2;

import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * @author: Patric Petscher <bitbucket.org/petschep>
 * created on 23.10.15.
 */
public class ue2_3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter any Integer Value");
        try{
            int x,y,z;
            x = input.nextInt();
            z = 0;
            System.out.println("Entered Int " + x);
            for (y=1; y<=x; y++){

                if(y%2 != 0){
                    z = z+y;
                    System.out.println("Number y ("+y+") is an odd number");
                }
            }
            System.out.println("Sum of odd numbers = " + z);
        }catch (InputMismatchException e){
            System.out.println("Error entered wrong type, restart Application!");
        }
    }
}
