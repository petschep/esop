package Lesson2;

import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * @author: Patric Petscher <bitbucket.org/petschep>
 * created on 25.10.15.
 */
public class ue2_9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.println("Enter any number");
            int i,n;
            n = input.nextInt();
            for (i=0; i<=n; i++){
                if(i%7 ==0 && i !=0){
                    System.out.println("number "+i+" can be devided by 7");
                }
            }
        }catch (InputMismatchException e){
            System.out.println("Error entered wrong type, restart Application!");
            System.out.println("Exeption: "+ e);
        }
    }
}
