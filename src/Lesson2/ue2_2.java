package Lesson2;

import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * @author: Patric Petscher <bitbucket.org/petschep>
 * created on 23.10.15.
 */
public class ue2_2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter any Integer Value");
        try{
            int x,y,z;
            x = input.nextInt();
            System.out.println("Entered Int " + x);
            for (y=1; y<=x; y++){
                z = x+y;
                System.out.println("Number "+y+"+"+x+"="+z);
            }
        }catch (InputMismatchException e){
            System.out.println("Error entered wrong type, restart Application!");
        }
    }
}
