package Lesson2;

import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * @author: Patric Petscher <bitbucket.org/petschep>
 * created on 04.11.15.
 */
public class ue2_12 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int i,n;

        System.out.println("Enter any number for maximum 100 times -> quit input with number zero");
        System.out.println("=====================================================================");
        System.out.println("");
        try{
            n = input.nextInt();
            i=0;
            int [] array = new int[n];
            while (i<n){
                array[i] = i;
                i++;
            }
            System.out.println("Array lenght is: "+n);
            printArray(array, false);
            printArray(array, true);

        }catch (InputMismatchException e){
            System.out.println("Error entered wrong type, restart Application!");
            System.out.println("Exeption: "+ e);
        }
    }


    /**
     * Return output of desired array
     * @param var
     * @param sort
     */
    public static void printArray(int[] var, boolean sort){
        String arrayOut = "";
        if(sort) {
            var = reverseSorting(var);
        }

        for (int i = 0; i <= var.length; i++){
            String placeholder;
            if(i ==0){
                placeholder = "[";
            }else if(i == var.length){
                placeholder = "]";
            }else{
                placeholder = " ,";
            }

            if(i == var.length){
                arrayOut = arrayOut+placeholder;
            }else{
                arrayOut = arrayOut+placeholder+" "+var[i];
            }

        }
        System.out.println(arrayOut);
    }

    /**
     * Reverse Array Sorting
     * @param nums
     * @return
     */
    public static int[] reverseSorting(int[] nums) {
        int[] reversed = new int[nums.length];
        for (int i=0; i<nums.length; i++) {
            reversed[i] = nums[nums.length - 1 - i];
        }
        return reversed;
    }
}
