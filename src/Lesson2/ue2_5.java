package Lesson2;

import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * @author: Patric Petscher <bitbucket.org/petschep>
 * created on 23.10.15.
 */
public class ue2_5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter any Integer Value");
        try{
            int i,x,y,z, sum;
            x = input.nextInt();
            if (x <= 0){
                sum = 0;
            } else if(x == 1){
                sum = 1;
            } else {
                y = 0;
                z = 1;
                i = 0;
                while (i<=x){
                    int yy = z; //Fib(i-1)
                    int zz = y+z; // Fib(i)
                    y = yy;
                    z = zz;
                    i++;
                    System.out.println("Fibonacci number "+z+" calculated in "+i+"' iteration");
                }
                sum = z;
            }
            System.out.println("========================= ");
            System.out.println("Fibonacci number is "+ sum);
        }catch (InputMismatchException e){
            System.out.println("Error entered wrong type, restart Application!");
        }
    }
}
