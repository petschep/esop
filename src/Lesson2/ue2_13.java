package Lesson2;

import java.util.Arrays;
import java.util.InputMismatchException;
import static java.lang.System.arraycopy;

/**
 * @author: Patric Petscher <bitbucket.org/petschep>
 * created on 04.11.15.
 */
public class ue2_13 extends  ue2_12{
    public static void main(String[] args) {
        int i;

        System.out.println("first array = [1,5,7] | second array = [2,4,6]");
        System.out.println("=====================================================================");
        System.out.println("");

        try{

            i=0;
            int [] arrayOne = {1,2,3};
            arrayOne =reverseSorting(arrayOne);
            //call Reversesorting Method to reverse Array
            reverseSorting(arrayOne);
            printArray(arrayOne, false);
            int [] arrayTwo = {4,5,7};
            arrayTwo =reverseSorting(arrayTwo);
            //call Reversesorting Method to reverse Array
            //reverseSorting(arrayTwo);
            //declare new array based on the length of the two given ones
            int [] arrayMerge = new int[arrayOne.length+arrayTwo.length];

            //merge arrays with arraycopy
            arraycopy( arrayOne, 0, arrayMerge, 0, arrayOne.length);
            arraycopy( arrayTwo, 0, arrayMerge, arrayOne.length, arrayTwo.length );

            System.out.println("Array lenght is: "+arrayMerge.length);
            printArray(arrayMerge, false);

        }catch (InputMismatchException e){
            System.out.println("Error entered wrong type, restart Application!");
            System.out.println("Exeption: "+ e);
        }
    }



}
