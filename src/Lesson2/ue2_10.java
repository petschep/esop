package Lesson2;

import java.util.Scanner;
import java.util.InputMismatchException;

/**
 * @author: Patric Petscher <bitbucket.org/petschep>
 * created on 26.10.15.
 */
public class ue2_10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            long perfectNumber = -1;

            do {
                System.out.println("Please enter a number to test if it is a perfect number");
                long x = input.nextInt();
                if(perfectInt(x)) perfectNumber = x;
            }while (perfectNumber == -1);
            System.out.println("Entered number "+perfectNumber+" is a perfect number");
            getDeviders(perfectNumber);



        }catch (InputMismatchException e){
            System.out.println("Error entered wrong type, restart Application!");
            System.out.println("Exeption: "+ e);
        }


    }

    /**
     * if given number equals the amount of deviders, the given number is a perfect number
     * @param x
     * @return
     */
    public static boolean perfectInt(long x) {
        return x == getSumOfDeviders(x);
    }

    /**
     * returns the sum of the divsors
     * @param sum
     * @return
     */
    public static long getSumOfDeviders(long sum){
        int sumOfDivisors = 1;
        for (int divisor = 2; divisor < sum; divisor++) {
            if ( sum % divisor == 0) {
                sumOfDivisors += divisor;
            }
        }
        return sumOfDivisors;
    }

    /**
     *
     * @param sum
     */
    public static void getDeviders(long sum){
        String sumOfDivisors = "1";
        for (int divisor = 2; divisor < sum; divisor++) {
            if ( sum % divisor == 0) {
                sumOfDivisors = sumOfDivisors+", "+divisor;
            }
        }
        System.out.println("Divisors are: "+sumOfDivisors);
    }
}
