package Lesson2;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 09.11.15.
 */
public class ue2_16 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int i,n,tmp;
        long [] myArray = new long[100];
        System.out.println("Enter n numbers | type 00 to exit");
        try{
            i=0;

            do {
                i++;
                System.out.println("Enter "+i+". number");
                n = input.nextShort();
                myArray[i]=n;
            }while (n != 00);

            for (i=0; i<10; i++){
                tmp = checkMatches(myArray, i);
                if(tmp >=1){
                    System.out.println("Number "+i+" appends "+tmp+ " times");
                }else{
                    continue;
                }
            }


        }catch (InputMismatchException e){
            System.out.println("Error entered wrong type, restart Application!");
            System.out.println("Exeption: "+ e);
        }


    }

    /**
     * Function to determine matches for given array and desired number
     * @param matches
     * @param num
     * @return
     */
    public static int checkMatches(long [] matches, int num){
        int counter = 0;
        int i;
        for (i = 0; i<matches.length; i++) {
            if(matches[i] == 0) continue;
            if(matches[i] == num) counter ++;
        }
        return counter;
    }
}
