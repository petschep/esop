package Lesson2;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 10.11.15.
 */
public class ue2_19 {
    public static void main(String[] args) {

        //declaring two boolean arrays to be compared
        boolean [] array1 = new boolean[]{true, true, true};
        boolean [] array2 = new boolean[]{true, false, true};

        System.out.println("Calculated hamming distance for given arrays is: "+ calcHammingDistance(array1, array2));
    }

    /**
     * Calc hamming distance by comparing two given arrays
     * @param a1
     * @param a2
     * @return
     */
    private static int calcHammingDistance(boolean[] a1,boolean[] a2){
        int hammingDist = 0;
        int counter = 0;
        int i;

        if(a1.length != a2.length){
            System.out.println("Arrays have no eqal lenght");
            return 0;
        }else{
            counter = a1.length;
        }
        for (i =0; i<counter; i++){
            if(a1[i] != a2[i]){
                hammingDist++;
            }
        }
        return hammingDist;
    }
}
