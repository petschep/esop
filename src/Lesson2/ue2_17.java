package Lesson2;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 10.11.15.
 */
public class ue2_17 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int i,index,n,tmp;
        int [] numbers = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
        System.out.println("Enter a number from {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15} to remove it");
        try{
            do {
                n = input.nextInt();
            }while (n<0 && n<=15);
            tmp = numbers.length;
            int [] newNumbers = new int[tmp-1];
            index=0;
            for (i=0; i < (tmp); i++){
                if(numbers[i] != n){
                    newNumbers[index++] = numbers[i];
                }else{
                    continue;
                }
            }
            System.out.println("new array is: " + Arrays.toString(newNumbers) );


        }catch (InputMismatchException e){
            System.out.println("Error entered wrong type, restart Application!");
            System.out.println("Exeption: "+ e);
        }


    }
}
