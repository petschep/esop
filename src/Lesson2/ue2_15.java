package Lesson2;

import java.util.Scanner;
import java.util.InputMismatchException;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 09.11.15.
 */
public class ue2_15 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        short i,n,counter;
        short [] myArray = new short[11];
        System.out.println("Enter 10 Numbers from type short");
        try{
            counter = 0;
            for (i=1; i<=10; i++){
                System.out.println("Enter "+i+". number");
                n = input.nextShort();
                if(n == 5) counter++;
                myArray[i]=n;
            }
            System.out.println("the array contains "+counter+ " elements with Nr. 5");

        }catch (InputMismatchException e){
            System.out.println("Error entered wrong type, restart Application!");
            System.out.println("Exeption: "+ e);
        }


    }
}
