package Lesson2;

import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * @author: Patric Petscher <bitbucket.org/petschep>
 * created on 24.10.15.
 */
public class ue2_8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int i,n;
        i=0;
        int [] array = new int[100];
        System.out.println("Enter any number for maximum 100 times -> quit input with number zero");
        System.out.println("=====================================================================");
        System.out.println("");
        try {
            do {
                System.out.println("Enter any number for index "+i);

                n = input.nextInt();
                array[i] = n;
                i++;

            } while (n != 0);

            System.out.println("Array entered is:");
            printArray(array);

        }catch (InputMismatchException e){
            System.out.println("Error entered wrong type, restart Application!");
        }
    }
    private static void printArray(int [] var){
        int max1, max2, indexMax1, indexMax2;
        max1 = var[0];
        max2 = var[1];
        indexMax2 = -1;
        indexMax1 = -1;
        for (int i = 0; i < var.length; i++) {
            if(var[i] != 0) {
                if(var[i]>max1)
                {
                    max2=max1;
                    max1=var[i];
                }else if(var[i]>max2){
                    max2 = var[i];
                }

                System.out.println(i + " => " + var[i]);
            }
        }
        for (int i = 0; i < var.length; i++){
            if(var[i] == max1) indexMax1 = i;
            if(var[i] == max2) indexMax2 = i;
        }

        System.out.println("highest value of array is " + max1 + " at the index of "+indexMax1);
        System.out.println("second highest value of array is " + max2 + " at the index of "+indexMax2);
    }
}
