package Lesson3;

//import java.util.Arrays;
import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 28.11.15.
 */
public class ue3_15 {
    public static void main(String[] args) {
        int size;
        int counter;
        Scanner input = new Scanner(System.in);
        try{
            System.out.println("Enter a number from 2-8 to calc your Chessboard");
            System.out.println("=============================");
            counter = 0;
            do {
                if(counter != 0){
                    System.out.println("You entered an incorrect number => try again:");
                }else{
                    System.out.println("Enter a Valid Number:");
                }
                size = input.nextInt();
                counter++;
            }while (size <= 1 || size >8);
            //call Chessboard with given Size
            String mychessboard = chessBoard(size);
            System.out.print(mychessboard);
        }catch(InputMismatchException e){
            System.out.println("Exeption e: "+e);
        }
    }

    /**
     * Returnes Chessboard
     * @param size
     */
    public static String  chessBoard(int size){
        int h, v;
        char [] abc = alphabeth();
        String out = "";
        //Loop through and return Line by line
        for(v = 0; v<= size;){
            //out = out+out;
            for (h=0; h<=size && v<=size; h++){
                out = out+" "+abc[h]+v;
            }
            out = out +"\n";
            v++;
        }
        return out;

    }

    /**
     * Method that returns the Alphaeth
     * @return
     */
    public static char[] alphabeth(){
        char[] alpha = new char[26];
        int k = 0;
        for(int i = 0; i < 26; i++){
            alpha[i] = (char)(97 + (k++));
        }
        return alpha;
    }
}


