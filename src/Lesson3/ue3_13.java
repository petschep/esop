package Lesson3;

import java.util.Arrays;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 28.11.15.
 */
public class ue3_13 {
    public static void main(String[] args) {
        int [] result;
        int [] result2;
        int [] array1 = new int[]{ 4,2,3 };
        int [] array2 = new int[]{ 1,5,3 };
        //pass arrays to mult fuction
        result = multArrays(array1, array2);
        //pass arrays to sub function
        result2= subArrays(array1,array2);
        System.out.println("Multiplied Array is: "+ Arrays.toString(result));
        System.out.println("Subtracted Array is: "+ Arrays.toString(result2));
    }

    /**
     * Mult the two given Arrays
     * @param var1
     * @param var2
     * @return
     */
    public static int [] multArrays(int[] var1, int[] var2){
        int l = var1.length;
        int i;
        if(l != var2.length) return null;
        int [] tmp = new int[l];
        for(i = 0; i<l; i++){
            tmp[i] = var1[i] * var2[i];
        }
        return tmp;
    }

    public static int [] subArrays(int[] var1, int[] var2){
        int l = var1.length;
        int i;
        if(l != var2.length) return null;
        int [] tmp = new int[l];
        for(i = 0; i<l; i++){
            tmp[i] = var1[i] - var2[i];
        }
        return tmp;
    }
}
