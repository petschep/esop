package Lesson3;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 16.11.15.
 */
public class ue3_2 {
    public static void main(String[] args) {
        int n1 = 4;
        int n2 = 5;
        int n3 = 6;
        int sum;
        sum = addition(n1,n2,n3);
        System.out.println("Sum is: "+sum );
    }
    public static final int addition(int n1, int n2, int n3){
        return n1+n2+n3;
    }
}
