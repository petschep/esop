package Lesson3;

import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 16.11.15.
 */
public class ue3_4 {
    public static void main(String[] args) {
        int x;
        int calc;
        Scanner input = new Scanner(System.in);
        try{
            System.out.println("Enter a various number:");
            x = input.nextInt();
            calc = method1(x);
            System.out.println("Calculated value is "+calc);
        }catch (InputMismatchException e){
            System.out.println("Exeption thrown: "+e);
        }
    }

    public static int method1(int value){
        return method2(value);
    }

    public static int method2(int value){
        return value*2;
    }
}
