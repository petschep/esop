package Lesson3;

import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 22.11.15.
 */
public class ue3_8 extends ue3_7{
    public static void main(String[] args) {
        int x;
        int i;
        String out;
        boolean primeNumber;
        Scanner input = new Scanner(System.in);
        try{
            x = input.nextInt();
            out = "";
            for(i=0; i<x; i++){
                if(isPrime(i)){
                    out = out+" "+i;
                }
            }
            System.out.println("Determined prime numbers are: "+out);
        }catch (InputMismatchException e){
            System.out.println("Exeption: "+e);
        }
    }
}
