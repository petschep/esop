package Lesson3;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 21.11.15.
 */
public class ue3_6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x;
        try{
            System.out.println("Enter a binary number:");
            x = input.nextInt();
            System.out.println("The calculated decimal number is: "+binToDez(x));


        }catch(InputMismatchException e){
            System.out.println("Error Exeption thrown: "+e);
        }
    }

    public static int binToDez(int value){
        return Integer.parseInt(String.valueOf(value), 2);
    }


}
