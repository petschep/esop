package Lesson3;

import java.nio.channels.Pipe;
import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 05.12.15.
 */
public class ue3_18 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int c = -1;
            int x;
            do {
                if(c != -1){
                    System.out.println("The number you entered is not valid, it must be an odd number");
                }else{
                    System.out.println("Enter an odd number");
                }
                x = input.nextInt();
                c++;
            }while (x%2!=1);
            System.out.println(""+ sandClock(x));

        }catch (InputMismatchException e){
            System.out.println("Exeption: "+e);
        }
    }

    /**
     * Returns Sandclock based on given number
     * @param n
     */
    public static String sandClock(int n){
        String o = "";
        int i;
        int max = n;
        for (i = 0; n>=i;){ //while n is bigger than i continue with loop
            for ( int t = 0;n>t; t++){ //define temp int t; while n is bigger than t raise t by one each time
                o = o + "*"; //add current count n to string for current n times
            }
            o = o+"\n"; //add a line break after each loop
            n--; //reduce amout of n and continue
        }
        for (i = 1; i <=max; i++ ) { //while n is bigger than i continue with loop
            for (int t = 2; t <i; t++) { //define temp int t; while n is bigger than t raise t by one each time
                o = o + "*"; //add current count n to string for current n times
            }
            o = o + "\n"; //add a line break after each loop
            //n--; //reduce amout of n and continue
        }
        return o;
    }
}
