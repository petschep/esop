package Lesson3;

import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 22.11.15.
 */
public class ue3_7 {
    public static void main(String[] args) {
        int x;
        boolean primeNumber;
        Scanner input = new Scanner(System.in);
        try{
            x = input.nextInt();
            primeNumber = isPrime(x);
            if(primeNumber){
                System.out.println("the entered number can be devided by itsself");
            }else{
                System.out.println("the entered number can NOT be devided by itsself");
            }
        }catch (InputMismatchException e){
            System.out.println("Exeption: "+e);
        }
    }
    public static boolean isPrime(int n) {
        for(int i=2;i<n;i++) {
            if(n%i==0)
                return false;
        }
        return true;
    }
}
