package Lesson3;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 15.11.15.
 */
public class ue3_1 {
    public static void main(String[] args) {
        noParam();
        getInt(3);
        int [] array = new int[] {1,2,3,4};
        getIntArray(array);
        char myChar = '\u03A6';
        getValues(myChar,array);
    }

    /**
     * method without param
     */
    public static void noParam() {

    }

    /**
     * Method with int param
     * @param i
     * @return
     */
    public static int getInt(int i){
        return i;
    }

    /**
     * Method with array param
     * @param array
     * @return
     */
    public static int[] getIntArray(int[] array){
        return array;
    }

    public static char getValues(char myChar, int[] myarray){
        return myChar;
    }


}
