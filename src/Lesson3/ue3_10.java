package Lesson3;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.math.BigInteger;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 22.11.15.
 */
public class ue3_10 {
    public static void main(String[] args) {
        int x;
        int i;
        Scanner input = new Scanner(System.in);
        try{
            System.out.println("Enter a positive number");
            x = input.nextInt();
            System.out.println("Calculated Factorial for following numbers are:");
            for(i=1; i<=x; i++){

                System.out.println("Factorial from "+i+" is "+ calcFactorial(i));
            }

        }catch (InputMismatchException e){
            System.out.println("Exeption: "+e);
        }
    }

    /**
     * Returnes calculated Factorial of number
     * @param val
     * @return
     */
    private static BigInteger calcFactorial(int val){
        int i;
        BigInteger sum = new BigInteger("1");
        BigInteger inc = new BigInteger("1");
        for (i=1; i<=val; i++){
            sum = sum.multiply(inc);
            inc = inc.add(BigInteger.ONE);
        }
        return sum;
    }
}
