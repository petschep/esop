package Lesson3;

import java.util.Arrays;
/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 16.11.15.
 */
public class ue3_3 {
    public static void main(String[] args) {
        int [] myArray = new int[] {1,2,3,4,5,6,7,8,9};
        int max = 4;

        int [] returnArray = teilArray(myArray,max);
        System.out.println("Returned Array is:"+ Arrays.toString(returnArray));

        int devider = 6;
        int devisor = 2;
        boolean devidable = teilt(devider, devisor);
        if(devidable){
            System.out.println( "Number can be devided by devisor");
        }else{
            System.out.println("Number cant be devided by divisor");
        }
    }

    /**
     * Returnes Array till index based on given int
     * @return
     */
    public static int[] teilArray(int [] n1, int n2){
        int [] temp = new int [n2];
        for (int i = 0; i<n2; i++){
            temp[i] =n1[i];
        }
        return temp;
    }

    /**
     * Checks if first int can be devided by second
     * @param n1
     * @param n2
     * @return
     */
    public static boolean teilt(int n1, int n2){
        return n1 % n2 == 0;
    }
}
