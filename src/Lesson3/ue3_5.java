package Lesson3;

import java.util.Scanner;
import java.util.InputMismatchException;
import java.lang.StringBuffer;
/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 21.11.15.
 */
public class ue3_5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x;
        try{
            System.out.println("Enter a positive number:");
            x = input.nextInt();
            System.out.println("The calculated binary number is: "+dezToBin(x));


        }catch(InputMismatchException e){
            System.out.println("Error Exeption thrown: "+e);
        }
    }

    public static StringBuffer dezToBin(int value){
        StringBuffer sBuf = new StringBuffer();
        int temp=0;
        while(value>0){
            temp = value%2;
            sBuf.append(temp);
            value = value / 2;
        }
        return sBuf.reverse();
    }


}
