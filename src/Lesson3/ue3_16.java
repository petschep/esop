package Lesson3;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 28.11.15.
 */
public class ue3_16 {
    public static void main(String[] args) {
        int n = 63;
        int i;
        //collect output as string
        String o = "";

        for (i = 0; n>=i;){ //while n is bigger than i continue with loop
            for ( int t = 0;n>t; t++){ //define temp int t; while n is bigger than t raise t by one each time
                o = o + " "+ n; //add current count n to string for current n times
            }
            o = o+"\n"; //add a line break after each loop
            n--; //reduce amout of n and continue
        }
        System.out.println(o);
    }
}
