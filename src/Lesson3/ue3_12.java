package Lesson3;

import java.util.Arrays;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 23.11.15.
 */
public class ue3_12 {
    public static void main(String[] args) {
        int[] array1 = new int[]{ 2,4,6,22,44,80}; // hier Werte eintragen bzw. die Methode aus Aufgabe 11 verwenden
        int[] array2 = new int[]{ 4,22,8,12,33,2}; // hier Werte eintragen bzw. die Methode aus Aufgabe 11 verwenden
        int[] erg = durchschnitt(array1, array2);
        if (erg == null){
            System.out.println("Die beiden Arrays haben keine gemeinsamen Elemente"); }
        else {
            System.out.println("Der Durchschnitt: "+ Arrays.toString(erg));
        }
    }
    public static int[] durchschnitt(int[] array1, int[] array2){
        int [] compared = new int[12];
        int c = 0;
        for(int i = 0; i < array1.length; i++) {
            for(int j = 0; j < array2.length; j++) {
                if(array1[i] == array2[j]) {
                    compared[c++] = array1[i];
                }
            }
        }
        if(compared.length >0){
            return compared;
        }else{
            return null;
        }

    }
}
