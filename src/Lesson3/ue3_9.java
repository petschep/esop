package Lesson3;

import Lesson2.ue2_10;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 22.11.15.
 */
public class ue3_9 extends ue2_10 {
    public static void main(String[] args) {
        long x;
        long i;
        String out;
        Scanner input = new Scanner(System.in);
        try{
            System.out.println("Enter a positive number");
            x = input.nextInt();
            out = "";
            for(i=0; i<x; i++){
                if(perfectInt(i)){
                    out = out+" "+i;
                }
            }
            System.out.println("Determined perfect numbers are: "+out);
        }catch (InputMismatchException e){
            System.out.println("Exeption: "+e);
        }
    }
}
