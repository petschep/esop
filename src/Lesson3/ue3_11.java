package Lesson3;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Arrays;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 22.11.15.
 */
public class ue3_11 {
    public static void main(String[] args) {
        int x;
        Scanner input = new Scanner(System.in);
        try{
            System.out.println("Enter a number");
            x = input.nextInt();

            System.out.println("Entered array is: "+Arrays.toString(buildArray(x)));


        }catch (InputMismatchException e){
            System.out.println("Exeption: "+e);
        }
    }

    public static int [] buildArray(int amount){
        int i;
        int [] tmp = new int[amount];
        Scanner input = new Scanner(System.in);

        for (i=0; i<amount; i++){
            try{
                System.out.println("Enter number "+(i+1)+" from "+amount);
                tmp[i] = input.nextInt();
            }catch (InputMismatchException e){
                System.out.println("Error: "+e);
            }
        }
        return tmp;
    }
}
