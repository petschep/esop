package Lesson1;

import java.util.Scanner;
import java.lang.Math;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 12.10.15.s
 */
public class ue1_12 {

    //Variables
    double x;
    double y;
    double z;

    public ue1_12(double x, double y, double z)
    {
        this.x = x; this.y = y; this.z = z;
    }

    public int calcTriangle()
    {
        if (this.x == this.y && this.y == this.z){
            return 1; //three of a kind
        } else if(this.x == this.y || this.z == this.y || this.x == this.z) {
            return  2; // two of a kind
        } else {
            double gamma  = calcAngle(this.x, this.y, this.z);
            double beta = calcAngle(this.x, this.z, this.x);
            // Alternative => Satz des Pythagoras für das rechtwinklige Dreieck
            // (c*c) = (b*b)+(a*a);
            if(gamma == 90 || beta == 90 || (180-gamma-beta) == 90) {
                return 3;
            }
        }

        return -1;

    }

    private double calcAngle(double x, double y, double z){
        // Achtung Klammer vor Punkt vor Strich, daher Klammern wie folgt getroffen

        return Math.toDegrees(Math.acos((Math.pow(x, 2) + Math.pow(y, 2) - Math.pow(z, 2)) / (2 * x * y)));
        //return Math.acos((x*x + y*y - z*z)/(2*x*y));
    }

    public static void main(String[ ] arg){

        //Declare Scanner
        System.out.println("Please enter the sides of the triangle:");
        Scanner input =  new Scanner(System.in);
        System.out.println("Enter variable x");
        double x = input.nextDouble();
        System.out.println("Enter variable y");
        double y = input.nextDouble();
        System.out.println("Enter variable z");
        double z = input.nextDouble();
        ue1_12 t = new ue1_12(x, y, z);
        int returnCode = t.calcTriangle();
        switch (returnCode){
            case 1:
                System.out.println("Triangle tree of a kind");
                break;
            case 2:
                System.out.println("Triangle two sides are equal");
                break;
            case 3:
                System.out.println("Triangle has a 90 degree angle");
                break;
            default:
                System.out.println("Mismatch for Triagle Variables");
                System.out.print(returnCode);
                break;
        }
        // return output based on methods return
        System.out.println();
    }
}
