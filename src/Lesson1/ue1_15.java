package Lesson1;

import java.util.Scanner;
import java.lang.Math;


/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 10.10.15.
 */
public class ue1_15 {
    public static void main(String[ ] arg){

        Scanner input =  new Scanner(System.in);
        System.out.println("Enter a b c & y");
        int a, b, c, y;

        a = input.nextInt();
        b = input.nextInt();
        c = input.nextInt();
        y = input.nextInt();

        System.out.println("Calculating...");

        quadraticEquations(a,b,c,y);
    }


    private static double quadraticEquations(int a, int b, int c, int y) {
        double x1;
        double x2;
        // After reforemed quadratic equations, determine x1 & x2
        x1 = (((-1) * b) - ( Math.sqrt( (Math.pow(b,2) - (4 * a * (c-y)) ) ) )/ (2 * a *b));
        x2 = (((-1) * b) + ( Math.sqrt( (Math.pow(b,2) - (4 * a * (c-y)) ) ) )/ (2 * a *b));
        if (x1 == 0 || x2 == 0) {
            System.out.println("Calculated x is: " + x1);
        }else if(x1 == ((-1)*x2)){
            System.out.println("Calculated x is: " + x1);
        }else{
            System.out.println("Calculated x1 is: "+x1 );
            System.out.println("Calculated x2 is: "+x2 );
        }

        return 0;
    }

}
