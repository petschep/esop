package Lesson1;

import java.util.Scanner;
import java.util.Calendar;
/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 09.10.15.
 */
public class ue1_17 {
    public static void main(String[ ] arg){

        Scanner input =  new Scanner(System.in);
        System.out.println("Enter your Loan in Euros");
        Calendar cal = Calendar.getInstance();
        int percent;
        int money = input.nextInt();


        switch (taxAmount(money)){
            case 1:
                percent = 10;
                System.out.println("Tax Amount: "+percent+"%");
                break;
            case 2:
                percent = 22;
                System.out.println("Tax Amount: "+percent+"%");
                break;
            case 3:
                percent = 32;
                System.out.println("Tax Amount: "+percent+"%");
                break;
            case 4:
                percent = 42;
                System.out.println("Tax Amount: "+percent+"%");
                break;
            default:
                percent = 0;
                System.out.println("No Valid Input");
                break;
        }
        System.out.println("Your final Loan would be: "+calcTaxes(money, percent)+"€");
    }

    private static int taxAmount(int money) {
        if(money >= 0 && money <= 5000) return 1;
        if(money >5000 && money <= 15000) return 2;
        if(money >15000 && money <= 30000) return 3;
        if(money >30000) return 4;
        return 0;
    }
    private static int calcTaxes(int money, int percent){
        int calculatecPercentage = money/100*percent;
        return money-calculatecPercentage;
    }

}
