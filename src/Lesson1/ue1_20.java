package Lesson1;
import java.util.Scanner;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 12.10.15.
 */
public class ue1_20 {
    private static int day;

    public static void main(String[] args) {
        Scanner input =  new Scanner(System.in);
        System.out.println("Enter number of weekday");
        int weekDay = input.nextInt();
        String day;
        switch (weekDay){
            case 1:
                day ="Montag";
            break;
            case 2:
                day = "Dienstag";
            break;
            case 3:
                day = "Mittwoch";
            break;
            case 4:
                day = "Donnerstag";
            break;
            case 5:
                day = "Freitag";
            break;
            case 6:
                day = "Samstag";
            break;
            case 7:
                day = "Sonntag";
            break;
            default:
                day = "Not a valid Number";
            break;
        }
        System.out.println("the desired day is:" + day);
    }
}
