package Lesson1;

import java.util.Scanner;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 07.10.15.
 */
public class ue1_10 {
    public static void main(String[ ] arg){
        int x;
        int y;
        int sum;
        Scanner input =  new Scanner(System.in);
        System.out.println("Enter variable x | must be an integer");
        x = input.nextInt();
        System.out.println("Enter variable y | must be an integer");
        y = input.nextInt();
        sum = ue1_10_calc(x, y);

        System.out.print("Calculated sum is "+sum);
    }
    private static int ue1_10_calc(int x, int y){
        //Task: Return 2x+y2
        return((x*2)+(y*y));
    }
}
