package Lesson1;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 06.10.15.
 */
public class ue1_8 {
    public static void main(String[ ] arg){
        int kilometer;
        double consumption;
        double consumptionPer100;
        kilometer = 50;
        consumption = 5.4;

        consumptionPer100 = (consumption * 100)/kilometer;

        System.out.print("Der Spritverbrauch für " + kilometer + "km ist: " + consumptionPer100+"l");
    }
}
