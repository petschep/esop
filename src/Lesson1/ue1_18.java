package Lesson1;

import java.util.Scanner;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 09.10.15.
 */
public class ue1_18 {
    public static void main(String[ ] arg){
        Scanner input =  new Scanner(System.in);
        System.out.println("Enter Year:");
        int year = input.nextInt();

        if(isLeapYear(year)){
            System.out.println("YES | the year " + year + " is a leap-year");
        }else {
            System.out.println("NO | the year " + year + " is no leap-year");
        }
    }

    private static boolean isLeapYear(int year) {
        return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
    }

}
