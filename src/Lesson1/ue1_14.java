package Lesson1;
import java.util.Scanner;
import java.lang.Math;
/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 18.10.15.
 */
public class ue1_14 {
    public static void main(String[] args) {
        double ccx;
        double ccy;
        double radius;
        double xpoint;
        double ypoint;

        Scanner input =  new Scanner(System.in);

        System.out.print("x koordinate der kreismitte:");
        ccx = input.nextDouble();
        System.out.print("y koordinate der kreismitte:");
        ccy = input.nextDouble();
        System.out.print("radius des kreises:");
        radius = input.nextDouble();
        System.out.print("x koordinate punkt: ");
        xpoint = input.nextDouble();
        System.out.print("y koordinate punkt: ");
        ypoint = input.nextDouble();

        double x1 = xpoint - ccx;
        double y1 = ypoint - ccy;
        double length = Math.sqrt(Math.pow(x1, 2) + Math.pow(y1, 2));

        if (length > radius) {
            System.out.println("punkt ist ausserhalb des kreises");
        } else if (length < radius) {
            System.out.println("punkt ist innerhalb des kreises");
        } else if( length == radius) {
            System.out.println("punkt ist direkt die mitte");
        } else {
            System.out.println("punkt irgendwo im kreis");
        }
    }
}

