package Lesson1;
import java.util.Scanner;
/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 12.10.15.
 */
public class ue1_19 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter desired money");
        int money = input.nextInt();
        int tempMoney = money;
        int m100= 0;
        int m50 = 0;
        int m20 = 0;
        int m10 = 0;
        int m5  = 0;

        while (tempMoney > 0){
            if(tempMoney < 5){
                System.out.println("it doesnt work with that number");
                continue;
            }
            m100 = money/100;
            tempMoney = tempMoney-(m100*100);
            m50 = tempMoney/50;
            tempMoney = tempMoney-(m50*50);
        }
        System.out.println("Money given: 100€:"+m100+"x, 50€:"+m50+"x, 20€:"+m20+"x, 10€:"+m10+"x, 5€:"+m5+"x");

    }
}
