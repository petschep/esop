package Lesson1;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 06.10.15.
 */
public class ue1_9 {
    public static void main(String[ ] arg){
        System.out.println("Method 1: " + method1());
        System.out.println("Method 2: " + method2());
        System.out.println("Method 3: " + method3());
        System.out.println("Method 4: " + method4());
        System.out.println("Method 5: " + method5());
        System.out.println("Method 6: " + method6());
        System.out.println("Method 7: " + method7());
    }

    /**
     * Aufgabe 1
     * @return
     */
    public static double method1(){
        /**
         * int a = 10;
         * float b = 1.1;
         * System.out.println(a+b);
         */
        int a = 10;
        double b = 1.1; //double erforderlich wegen data loss
        return a+b;//System.out.println(a+b);

    }

    /**
     * Aufgabe 2
     * @return
     */
    public static double method2(){
        /**
         * int a = 10;
         * double b = 1.1;
         * int c = a+b;
         * System.out.println(c);
         */
        int a = 10;
        double b = 1.1;
        double c = a+b;
        return c;
    }

    /**
     * Aufgabe 3
     * @return
     */
    public static int method3(){
        /**
         * int a = 10;
         * double b = 1.1;
         * int c = a+(int)b; System.out.println(c);
         */
        int a = 10;
        double b = 1.1;
        int c = a+(int)b;
        return  c; // Korrekt,jedoch wird auf die Nachkommastelle vergesse, bzw diese gerundet durch casten zu int.
    }

    /**
     * Aufgabe 4
     * @return
     */
    public static double method4(){
        /**
         * float a = 0;
         * double b = 1.1;
         * double c = a+b;
         * System.out.println(c);
         */
        float a = 0;
        double b = 1.1;
        double c = a+b;
        return c; //Korrekt lt Angabe
    }

    /**
     * Aufgabe 5
     * @return
     */
    public static int method5(){
        /**
         * float a = 0.9;
         * double b = 1.1;
         * int c = (int)a+(int)b;
         * System.out.println(c);
         */
        double a = 0.9; //muss double sein weil kein F/f am Ende
        double b = 1.1;
        int c = (int)a+(int)b;
        return c; //Funktioniert, allerdings erfolgt Datenverlust durch casten der var a+b zu int folglich 1+0 = 1
    }

    /**
     * Aufgabe 6
     * @return
     */
    public static int method6(){
        /**
         * float a = 0;
         * double b = 1.1;
         * int c = (int)b / (int)a;
         * System.out.println(c);
         */
        float a = 0;
        double b = 1.1;
        int c = (int)a / (int)b ; //Exeption wegen division durch 0
        return c;
    }

    /**
     * Aufgabe 7
     * @return
     */
    public static String method7(){
        /**
         * int a = 45;
         * int b = a;
         * int c = Integer.MAX_VALUE+1;
         * float e = 4.5;
         * a = a*a;
         * b = ((b*b)/b)%b);
         * System.out.println("a="+a+" b="+b+" c="+c);
         */
        int a = 45;
        int b = a;
        int c = Integer.MAX_VALUE+1;
        double e = 4.5; //type change auf double
        a = a*a;
        b = (b % b); // Gekürzte Ausgabe, da b*b/b = b ist.
        return "a="+a+" b="+b+" c="+c;
    }

}
