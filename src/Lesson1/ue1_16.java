package Lesson1;

import java.util.Scanner;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 09.10.15.
 */
public class ue1_16 {
    public static void main(String[ ] arg){

        Scanner input =  new Scanner(System.in);
        System.out.println("Enter points of student");
        int score = input.nextInt();

        switch (gradeCalculator(score)){
            case 5:
                System.out.println("Grade: 5");
                break;
            case 4:
                System.out.println("Grade: 4");
                break;
            case 3:
                System.out.println("Grade: 3");
                break;
            case 2:
                System.out.println("Grade: 2");
                break;
            case 1:
                System.out.println("Grade: 1");
                break;
            default:
                System.out.println("No Valid Input for Gradesystem");
                break;

        }
    }

    private static int gradeCalculator(int score) {
        if(score >= 0 && score <= 200) return 5;
        if(score >200 && score <= 250) return 4;
        if(score >250 && score <= 300) return 3;
        if(score >300 && score <= 350) return 2;
        if(score >350 && score <= 400) return 1;
        return 0;
    }

}
