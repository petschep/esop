package Lesson1;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 06.10.15.
 */
public class ue1_7 {
    public static void main(String[ ] arg){
        int radius;
        double pi;
        double circumference;

        radius = 200;
        pi = java.lang.Math.PI;
        //U = π · r · 2
        circumference = pi * radius * 2;

        System.out.print("Das Ergebnis des Kreisumfanges ist " + circumference);
    }
}
