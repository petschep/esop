package Lesson1;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 05.10.15.
 */
public class ue1_4 {
    public static void main(String[ ] arg){

        /**
         * Task:
         * Legen Sie geeignete Variablen für die Werte:
         * -32, 6.24f, 1.968, 1.00e+3f, 1.00e-3, 1624, "1000", "A", 'A', false, '\n', “\n“, 0xA5, '\u0065', -25874 an,
         * weisen Sie Ihnen die Werte zu und geben Sie die Variablen aus
         */
        byte    var1 = -32;
        float   var2 = 6.24f;
        double  var3 = 1.968;
        String  var4 = "1.00ef";
        short   var5 = 1624;
        String  var6 = "\"1000\"";
        String  var7 = "A";
        char    var8 = 'A';
        boolean var9 = false;
        char    var10 = '\n';
        String  var11 = "“\n“";
        String  var12 = "0xA5";
        char    var13 = '\u0065';
        short   var14 = -25874;

        //print int
        System.out.println(var1);
        System.out.println(var5);
        System.out.println(var14);
        //print float
        System.out.print(var3);
        //print char
        System.out.println(var8);
        System.out.println(var10);
        System.out.println(var13);
        //print strings
        System.out.println(var2);
        System.out.println(var4);
        System.out.println(var6);
        System.out.println(var7);
        System.out.println(var11);
        System.out.println(var12);

    }
}
