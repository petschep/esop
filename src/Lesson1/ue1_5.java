package Lesson1;

/**
 * @author: Patric Petscher <petschep@gmail.com>
 * created on 05.10.15.
 */
public class ue1_5 {
    public static void main(String[ ] arg){
        int x;
        int y;
        int sum;

        x = 5;
        y = 14;
        sum = ue1_5_calc(x, y);

        System.out.print(sum);
    }
    private static int ue1_5_calc(int x, int y){
        //Task: Return 2x+y2
        return((x*2)+(y*y));
    }
}
