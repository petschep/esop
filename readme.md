#ESOP Übungseinheiten

### Alle Bsp. der ESOP Uebungseinheit

Pulled oder Forked das Repo gerne, oder schreibt Tipps als Kommentar oder Tickets.    
Hoffe es hilft euch etwas


----

Im "*src*" Ordner sind alle Beispiele nach Lessons aufgeteilt.

| Aufgabenblatt        | Folder           | LINK  |
| ------------- |:-------------:| -----:|
| Aufgabenblatt 1 | Lesson1 | [UE #1](https://bitbucket.org/petschep/esop/src/99e662d2ba7cb8b1eb8e5df3bc1a229c1d04c2e4?at=UE1)|
| Aufgabenblatt 2 | Lesson2 | [UE #2](https://bitbucket.org/petschep/esop/src/?at=master) |

![Java Logo](http://winxperts4all.at/images/stories/artikelbilder/Windows_Phone/java-logo.png "Logo Java Source Wikipedia")